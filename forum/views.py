from django.shortcuts import get_object_or_404,render,redirect,reverse
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse
from django.views import generic
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .forms import LoginForm

# Create your views here.

#from .models import *
# Create your views here.
class HomeView(generic.TemplateView):
    template_name = 'index.html'
    

class LoginFormView(generic.View):
	template_name = 'registrations/login.html'
	form_class = LoginForm
	def get(self, request):
		if request.user.is_authenticated:
			return redirect(reverse('userlist'))
		form = self.form_class(None)
		return render(request,self.template_name, {'form':form})

	def post(self, request):
		if request.user.is_authenticated:
			return redirect(reverse('home'))
		form = self.form_class(None)
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username=username,password=password)
		if user is not None:
			if user.is_active:
				login(request,user)
				return redirect(reverse('home'))
		return render(request,self.template_name,{"form":form})

def logoutpage(request):
	logout(request)
	return redirect(reverse('home'))