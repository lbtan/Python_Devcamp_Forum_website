from django.conf.urls import url
from django.views.generic.base import RedirectView
from . import views

urlpatterns = [
    url(r'^$', RedirectView.as_view(permanent=False,pattern_name='home')),
    url(r'^login/', views.LoginFormView.as_view(), name='login'),
    url(r'^logout/', views.logoutpage, name='logout'),
    url(r'^home/', views.HomeView.as_view(), name='home')
]